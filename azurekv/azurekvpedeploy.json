{
  "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
  "contentVersion": "1.0.0.0",
  "parameters": {
    "location": {
      "type": "string",
      "defaultValue": "[resourceGroup().location]",
      "metadata": {
        "description": "Specifies the location for all the resources."
      }
    },
    "virtualNetworkName": {
      "defaultValue": "ubsvnet",
      "type": "string",
      "metadata": {
        "description": "Specifies the name of the virtual network hosting the virtual machine."
      }
    },
    "virtualNetworkAddressPrefix": {
      "defaultValue": "10.0.0.0/16",
      "type": "string",
      "metadata": {
        "description": "Specifies the address prefix of the virtual network hosting the virtual machine."
      }
    },
    "subnetName": {
      "defaultValue": "DefaultSubnet",
      "type": "string",
      "metadata": {
        "description": "Specifies the name of the subnet hosting the virtual machine."
      }
    },
    "subnetAddressPrefix": {
      "defaultValue": "10.0.0.0/24",
      "type": "string",
      "metadata": {
        "description": "Specifies the address prefix of the subnet hosting the virtual machine."
      }
    },
    "keyVaultName": {
      "type": "string",
      "metadata": {
        "description": "Specifies the name of the key vault."
      }
    },
    "keyvaultlocation": {
      "type": "string",
      "defaultValue": "[resourceGroup().location]",
      "metadata": {
        "description": "Specifies the Azure location where the key vault should be created."
      }
    },
    "enabledForDeployment": {
      "type": "bool",
      "defaultValue": false,
      "allowedValues": [
        true,
        false
      ],
      "metadata": {
        "description": "Specifies whether Azure Virtual Machines are permitted to retrieve certificates stored as secrets from the key vault."
      }
    },
    "enabledForDiskEncryption": {
      "type": "bool",
      "defaultValue": false,
      "allowedValues": [
        true,
        false
      ],
      "metadata": {
        "description": "Specifies whether Azure Disk Encryption is permitted to retrieve secrets from the vault and unwrap keys."
      }
    },
    "enabledForTemplateDeployment": {
      "type": "bool",
      "defaultValue": false,
      "allowedValues": [
        true,
        false
      ],
      "metadata": {
        "description": "Specifies whether Azure Resource Manager is permitted to retrieve secrets from the key vault."
      }
    },
    "tenantId": {
      "type": "string",
      "metadata": {
        "description": "Specifies the Azure Active Directory tenant ID that should be used for authenticating requests to the key vault. Get it by using Get-AzSubscription cmdlet."
      }
    },
    "objectId": {
      "type": "string",
      "metadata": {
        "description": "Specifies the Application (client) ID of a user, service principal or security group in the Azure Active Directory tenant for the vault. The object ID must be unique for the list of access policies. Get it by using Get-AzADUser or Get-AzADServicePrincipal cmdlets."
      }
    },
    "keysPermissions": {
      "type": "array",
      "defaultValue": [
        "get",
        "list",
        "create"
      ],
      "metadata": {
        "description": "Specifies the permissions to keys in the vault. Valid values are: all, encrypt, decrypt, wrapKey, unwrapKey, sign, verify, get, list, create, update, import, delete, backup, restore, recover, and purge."
      }
    },
    "secretsPermissions": {
      "type": "array",
      "defaultValue": [
        "get",
        "list"
      ],
      "metadata": {
        "description": "Specifies the permissions to secrets in the vault. Valid values are: all, get, list, set, delete, backup, restore, recover, and purge."
      }
    },
    "certificatesPermissions": {
      "type": "array",
      "defaultValue": [
        "get",
        "list",
        "create"
      ],
      "metadata": {
        "description": "Specifies the permissions to certificates in the vault. Valid values are: all, get, list, set, delete, backup, restore, recover, and purge."
      }
    },
    "storagePermissions": {
      "type": "array",
      "defaultValue": [
        "get",
        "list"
      ],
      "metadata": {
        "description": "Specifies the permissions to storage in the vault. Valid values are: all, get, list, set, delete, backup, restore, recover, and purge."
      }
    },
    "skuName": {
      "type": "string",
      "defaultValue": "Standard",
      "allowedValues": [
        "Standard",
        "Premium"
      ],
      "metadata": {
        "description": "Specifies whether the key vault is a standard vault or a premium vault."
      }
    },
    "keyVaultPrivateEndpointName": {
      "type": "string",
      "defaultValue": "KeyVaultPrivateEndpoint",
      "metadata": {
        "description": "Specifies the name of the private link to key vault."
      }
    },
    "CLIoutput": {
      "type": "string",
      "defaultValue": "CLIoutputforKeyVaultresourceId",
      "metadata": {
        "description": "Specifies the name of the private link to key vault."
      }
    },
    "secretName": {
      "type": "string",
      "metadata": {
        "description": "Specifies the name of the secret that you want to create."
      }
    },
    "secretValue": {
      "type": "securestring",
      "metadata": {
        "description": "Specifies the value of the secret that you want to create."
      }
    }
 },
  "variables": {
    "subnetId": "[resourceId('Microsoft.Network/virtualNetworks/subnets', parameters('virtualNetworkName'), parameters('subnetName'))]",
    "vnetId": "[resourceId('Microsoft.Network/virtualNetworks', parameters('virtualNetworkName'))]",
    "keyVaultId": "[resourceId('Microsoft.KeyVault/vaults', parameters('keyVaultName'))]",
    "keyVaultPrivateEndpointGroupName": "vault"
  },
  "resources": [
    {
      "apiVersion": "2019-09-01",
      "type": "Microsoft.KeyVault/vaults",
      "name": "[parameters('keyVaultName')]",
      "location": "[parameters('keyvaultlocation')]",
      "properties": {
        "enabledForDeployment": "[parameters('enabledForDeployment')]",
        "enabledForDiskEncryption": "[parameters('enabledForDiskEncryption')]",
        "enabledForTemplateDeployment": "[parameters('enabledForTemplateDeployment')]",
        "tenantId": "[parameters('tenantId')]",
        "accessPolicies": [
          {
            "objectId": "[parameters('objectId')]",
            "tenantId": "[parameters('tenantId')]",
            "permissions": {
              "keys": "[parameters('keysPermissions')]",
              "secrets": "[parameters('secretsPermissions')]",
              "certificates": "[parameters('certificatesPermissions')]",
              "storage": "[parameters('storagePermissions')]"
            }
          }
        ],
        "sku": {
          "name": "[parameters('skuName')]",
          "family": "A"
        },
        "networkAcls": {
          "defaultAction": "Allow",
          "bypass": "AzureServices"
        }
      }
    },
    {
      "apiVersion": "2019-11-01",
      "type": "Microsoft.Network/virtualNetworks",
      "name": "[parameters('virtualNetworkName')]",
      "location": "[parameters('location')]",
      "properties": {
        "addressSpace": {
          "addressPrefixes": [
            "[parameters('virtualNetworkAddressPrefix')]"
          ]
        },
        "subnets": [
          {
            "name": "[parameters('subnetName')]",
            "properties": {
              "addressPrefix": "[parameters('subnetAddressPrefix')]",
              "privateEndpointNetworkPolicies": "Disabled",
              "privateLinkServiceNetworkPolicies": "Enabled"
            }
          }
        ]
      }
    },
    {
      "type": "Microsoft.Network/privateEndpoints",
      "apiVersion": "2020-04-01",
      "name": "[parameters('keyVaultPrivateEndpointName')]",
      "location": "[parameters('location')]",
      "dependsOn": [
        "[variables('vnetId')]",
        "[variables('keyVaultId')]"
      ],
      "properties": {
        "privateLinkServiceConnections": [
          {
            "name": "[parameters('keyVaultPrivateEndpointName')]",
            "properties": {
              "privateLinkServiceId": "[variables('keyVaultId')]",
              "groupIds": [
                "[variables('keyVaultPrivateEndpointGroupName')]"
              ]
            }
          }
        ],
        "subnet": {
          "id": "[variables('subnetId')]"
        }
      }
    },
    {
      "type": "Microsoft.Resources/deploymentScripts",
      "apiVersion": "2019-10-01-preview",
      "name": "[parameters('CLIoutput')]",
      "location": "[resourceGroup().location]",
      "kind": "AzureCLI",
      "identity": {
        "type": "UserAssigned",
        "userAssignedIdentities": {
        "/subscriptions/1725df1b-1ef4-439b-be27-b662a0747201/resourceGroups/azureubs1/providers/Microsoft.ManagedIdentity/userAssignedIdentities/UBSidentity": 
        {}
        }
      },
      "properties": {
        "AzCliVersion": "2.0.80",
        "timeout": "PT30M",
        "scriptContent": "result1=$(az keyvault show -n ubs-kv-name-random-14 | jq '.id');result2=$(az keyvault show -n ubs-kv-name-random-14 | jq '.properties.privateEndpointConnections[0].privateEndpoint.id');output=\"$result1, $result2\";Write-Output $output;$DeploymentScriptOutputs = @{};$DeploymentScriptOutputs['text'] = $output",
        "cleanupPreference": "OnSuccess",
        "retentionInterval": "P1D"
      }
    }
  ],
  "outputs": {
    "keyVaultPrivateEndpoint": {
      "value": "[reference(resourceId('Microsoft.Network/privateEndpoints', parameters('keyVaultPrivateEndpointName')), '2020-04-01', 'Full')]",
      "type": "object"
    },
    "keyVault": {
      "value": "[reference(resourceId('Microsoft.KeyVault/vaults', parameters('keyVaultName')), '2019-09-01', 'Full')]",
      "type": "object"
    },
    "deploymentScripts": {
      "value": "[reference(parameters('CLIoutput')).outputs.text]",
      "type": "string"
    }
  }
}